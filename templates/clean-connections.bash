#!/bin/bash

TYPE=$1
NAME=$2
STATE=$3
LOGFILE=/var/log/keepalived-transitions.log

echo -n "[$(date)] Entering $STATE state." >> $LOGFILE

# List any open connections that HAProxy has.
lsof -n -iTCP -sTCP:ESTABLISHED -a -c haproxy

# If there are any open connections, clean them up.
if [[ 0 == $? ]]; then
        echo " There are established connections. Restart HAProxy to clean them up." >> $LOGFILE
        systemctl restart haproxy.service
else
        echo " No connections to clean up." >> $LOGFILE
fi
