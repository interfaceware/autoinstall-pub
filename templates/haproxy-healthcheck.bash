#!/usr/bin/env bash
#
# Determine if HAProxy is running.

if [ ! -f /var/run/haproxy.pid ]; then
        exit 1
else
        ps -p `cat /var/run/haproxy.pid` > /dev/null
        exit $?
fi
